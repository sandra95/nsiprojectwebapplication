// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import Axios from 'axios'
import 'vuetify/dist/vuetify.min.css'
export const serverBus = new Vue();
import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader
import firebase from 'firebase'
Vue.use(Vuetify, {
  iconfont: 'fa'
})

Vue.config.productionTip = false
Vue.use(Axios)
var config = {
  apiKey: "AIzaSyCsz15WkNzq4XT_RitB7KDTAfwSLCfRnzE",
  authDomain: "nsiproject.firebaseapp.com",
  databaseURL: "https://nsiproject.firebaseio.com",
  projectId: "nsiproject",
  storageBucket: "nsiproject.appspot.com",
  messagingSenderId: "106636057359"
};
firebase.initializeApp(config);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
