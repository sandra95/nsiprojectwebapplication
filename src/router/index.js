import Vue from 'vue'
import Router from 'vue-router'
import MapComponent from '@/components/mapComponent'
import LoginComponent from '@/components/loginComponent'
import ContactComponent from '@/components/contactComponent'
import CommentComponent from '@/components/commentsComponent'
import ProfileComponent from '@/components/userProfileComponent'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MapComponent',
      component: MapComponent
    },
    {
      path: '/login',
      name: 'LoginComponent',
      component: LoginComponent
    },
    {
      path: '/contact',
      name: 'ContactComponent',
      component: ContactComponent
    },
    {
      path: '/comments/:name',
      name: 'CommentComponent',
      component: CommentComponent
    },
    {
      path: '/profile',
      name: 'ProfileComponent',
      component: ProfileComponent
    }
  ]
})
